package com.sc.client;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/home")
public class UserController {
    @Value("${some.words}")
    String words;
    @RequestMapping("/welcome")
    public String home(@RequestParam String name) {
        return name+" 欢迎您，这里有一些话想对你说:    " +words;
    }
}
