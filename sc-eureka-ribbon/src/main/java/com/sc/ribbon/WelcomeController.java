package com.sc.ribbon;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/home")
public class WelcomeController {
    @Autowired
    WelcomeService welcomeService;
    @RequestMapping(value = "/welcome")
    public String welcome(@RequestParam String name){
        System.out.println("ribbon=================");
        return welcomeService.welcomeService(name);
    }
}
