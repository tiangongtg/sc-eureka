package com.sc.bus;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RefreshScope
public class UserController {
    @Value("${username}")
    String username;
    @Value("${password}")
    String password;
    @RequestMapping(value = "/user")
    public String user(){
        return "您好，您用户名为："+username+",密码为："+password;
    }
}
