package com.sc.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(value = "sc-eureka-client",fallback = WelcomeError.class)
public interface WelcomeInterface {
    @RequestMapping(value = "/home/welcome",method = RequestMethod.GET)
    String welcomeClientOne(@RequestParam(value = "name") String name);
}
