package com.sc.feign;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/home")
public class WelcomeController {
    @Autowired
    WelcomeInterface welcomeInterface;
    @RequestMapping(value = "/welcome",method = RequestMethod.GET)
    public String welcome(@RequestParam String name){
        System.out.println("feign============");
        return welcomeInterface.welcomeClientOne(name);
    }
}
