package com.sc.feign;

import org.springframework.stereotype.Component;

@Component
public class WelcomeError implements WelcomeInterface {
    @Override
    public String welcomeClientOne(String name) {
        return "哎呀呀,不好意思"+name+",出错了呀!";
    }
}
